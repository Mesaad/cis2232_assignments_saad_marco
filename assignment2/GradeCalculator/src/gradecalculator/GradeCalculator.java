/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradecalculator;

import java.util.Iterator;
import java.util.Scanner;


public class GradeCalculator {

    public DeliverableManager dm = new DeliverableManager();
    public Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) {
        GradeCalculator gc = new GradeCalculator();
        
        //if(gc.dm.loadFromFile())
        //{
        //    System.out.println("Load from File successful.");
        //}
        if (gc.dm.loadFromDataBase())
        {
            System.out.println("Load from Database successful.");
        }
        
        
        gc.run();
    }
    
    public void displayMenu()
    {
        System.out.println("L - Load from Input");
        System.out.println("S - Show deliverable");
        System.out.println("G - Calculate Grade");
        System.out.println("E - Exit");
    }
    
    public void run()
    {
        String choice = "";
        while (!choice.equals("E"))
        {
            displayMenu();
            choice = sc.nextLine();
            if(choice.equals("L"))
                dm.loadFromUserInput();
            if(choice.equals("S"))
            {
                for (Iterator<Deliverable> it = dm.deliverables.iterator(); it.hasNext();)
                {
                    System.out.println(it.next());
                }
            }
            if(choice.equals("G"))
            {
                doCalculations();
            }
        }    
    }
    
    public void doCalculations()
    {
        int average = 0;
        for (Iterator<Deliverable> it = dm.deliverables.iterator(); it.hasNext();)
        {
            Deliverable d = it.next();
            System.out.println(d);
            System.out.println("Mark?");
            average += sc.nextInt() * d.weight;
            sc.nextLine();
        }
        System.out.println("Average = " + (average / 100));
    }
}
