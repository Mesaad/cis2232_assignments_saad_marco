/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradecalculator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;



public class DeliverableManager {

    public ArrayList<Deliverable> deliverables;
    
    public DeliverableManager()
    {
        deliverables = new ArrayList<Deliverable>();
    }
    
    public boolean loadFromFile()
    { 
        try
        {
            deliverables = new ArrayList<Deliverable>();
            BufferedReader inputStream = new BufferedReader(new FileReader("c:\\cis2232\\deliverables.csv"));
            String fileLine = inputStream.readLine();
            while (fileLine != null)
            {
                String name = fileLine.split(",")[0];
                int weight = Integer.parseInt(fileLine.split(",")[1]);
                deliverables.add(new Deliverable(name,weight));
                fileLine = inputStream.readLine();
            }
            inputStream.close();
            return true;
        }
        catch (Exception e) 
        {
            return false;
        }
    }
    
    public void loadFromUserInput()
    {
        deliverables = new ArrayList<Deliverable>();
        int totalWeight = 0;
        while (totalWeight < 100) {
            System.out.println("Total Weight = " + totalWeight);
            Scanner sc = new Scanner(System.in);
            System.out.println("Name?");
            String name = sc.nextLine();
            System.out.println("Weight");
            int weight;
            weight = sc.nextInt();
            sc.nextLine();
            while (weight + totalWeight > 100) {
                System.out.println("Invalid Weight.");
                weight = sc.nextInt();
                sc.nextLine();
            }
            deliverables.add(new Deliverable(name, weight));
            totalWeight += weight;
        }
    }
    
    public boolean loadFromDataBase()
    {
        deliverables = new ArrayList<Deliverable>();
        try
        {
            Connection conn = DatabaseUtility.getConnection();
            Statement stmt = conn.createStatement();
            String sql = "SELECT Name, Weight FROM Deliverable";
            ResultSet rs = stmt.executeQuery(sql); 
            while(rs.next())
            {
                deliverables.add(new Deliverable(rs.getString("Name"), rs.getInt("Weight")));
            }
            conn.close();
            return true;
        }
        catch(SQLException se)
        {
            se.printStackTrace();
        }
        return false;
    }
    
    
}
