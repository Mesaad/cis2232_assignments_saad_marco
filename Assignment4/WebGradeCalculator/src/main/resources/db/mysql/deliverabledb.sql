CREATE DATABASE deliverabledb;
use deliverabledb;
Drop Table Deliverable;
CREATE TABLE Deliverable (id int AUTO_INCREMENT, name varchar(20), weight int, CONSTRAINT id_pk PRIMARY KEY (id));
             
INSERT INTO Deliverable (id, name, weight) VALUES (1,"Assignment 1", 20);
INSERT INTO Deliverable (id, name, weight) VALUES (2,"Assignment 2", 20);
INSERT INTO Deliverable (id, name, weight) VALUES (3,"Final Exam", 60);
