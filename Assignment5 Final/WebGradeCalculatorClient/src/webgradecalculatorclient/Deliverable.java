package webgradecalculatorclient;

import java.io.Serializable;



public class Deliverable implements Serializable {

 
    public int id;
    public String name;
    public int weight;

    public Deliverable(int id, String name, int weight) {
        this.id = id;
        this.name = name;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public Deliverable(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }
    
    public Deliverable(){
        this.name = "";
        this.weight = 0;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
    
    @Override
    public String toString() {
        return "Deliverable{id=" + id + ", name=" + name + ", weight=" + weight + '}';
    }
    
    
}
