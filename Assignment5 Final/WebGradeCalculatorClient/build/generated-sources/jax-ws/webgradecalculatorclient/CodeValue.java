
package webgradecalculatorclient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for codeValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="codeValue">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codeTypeId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codeValueSequence" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="englishDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="englishDescriptionShort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="testInt" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "codeValue", propOrder = {
    "codeTypeId",
    "codeValueSequence",
    "englishDescription",
    "englishDescriptionShort",
    "testInt"
})
public class CodeValue {

    protected int codeTypeId;
    protected int codeValueSequence;
    protected String englishDescription;
    protected String englishDescriptionShort;
    @XmlElement(nillable = true)
    protected List<Integer> testInt;

    /**
     * Gets the value of the codeTypeId property.
     * 
     */
    public int getCodeTypeId() {
        return codeTypeId;
    }

    /**
     * Sets the value of the codeTypeId property.
     * 
     */
    public void setCodeTypeId(int value) {
        this.codeTypeId = value;
    }

    /**
     * Gets the value of the codeValueSequence property.
     * 
     */
    public int getCodeValueSequence() {
        return codeValueSequence;
    }

    /**
     * Sets the value of the codeValueSequence property.
     * 
     */
    public void setCodeValueSequence(int value) {
        this.codeValueSequence = value;
    }

    /**
     * Gets the value of the englishDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnglishDescription() {
        return englishDescription;
    }

    /**
     * Sets the value of the englishDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnglishDescription(String value) {
        this.englishDescription = value;
    }

    /**
     * Gets the value of the englishDescriptionShort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnglishDescriptionShort() {
        return englishDescriptionShort;
    }

    /**
     * Sets the value of the englishDescriptionShort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnglishDescriptionShort(String value) {
        this.englishDescriptionShort = value;
    }

    /**
     * Gets the value of the testInt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the testInt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTestInt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getTestInt() {
        if (testInt == null) {
            testInt = new ArrayList<Integer>();
        }
        return this.testInt;
    }

}
