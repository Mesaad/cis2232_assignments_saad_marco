/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.web.services;

import info.hccis.admin.dao.DeliverableDAO;
import info.hccis.admin.model.DatabaseConnection;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;


@WebService(serviceName = "WebServiceDeliverable")
public class WebServiceDeliverable {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "list1")
    public ArrayList<info.hccis.admin.model.jpa.Deliverable> list() {
        DatabaseConnection dbc = new DatabaseConnection();
        return DeliverableDAO.getDeliverables(dbc);
    }

    /**
     * Web service operation
     */
   

    
    



    
}
