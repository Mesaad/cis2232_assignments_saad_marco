package info.hccis.admin.web.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/useraccess")
public class UserAccessRest {

    @GET
    @Path("/{param}")
    public Response getMsg(@PathParam("param") String msg) {
        try {
            String[] theParts = msg.split(",");
            String database = theParts[0];
            String username = theParts[1];
            String password = theParts[2];
        } catch (Exception e) {
            System.out.println("In access web service.  There was an error with the input.");
        }
        String output = "1";
//                boolean valid = true;
        return Response.status(200).entity(output).build();

    }

    @GET
    @Path("/getPasswordHash/{param}")
    public Response getPasswordHash(@PathParam("param") String passwordIn) {

        String password = passwordIn;

        String output = password + "Hash";
//                boolean valid = true;
        return Response.status(200).entity(output).build();

    }

}
