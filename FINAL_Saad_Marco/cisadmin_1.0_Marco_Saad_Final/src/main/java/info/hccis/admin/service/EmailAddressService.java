/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service;

import info.hccis.admin.dao.UserAccessDAO;
import info.hccis.admin.model.DatabaseConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Marco Saad
 * Created to get Email Address
 * 
 */
@WebService(serviceName = "EmailAddressService")
public class EmailAddressService {

    @WebMethod(operationName = "getEmailAddress")
    public String getEmailAddress(@WebParam (name = "databaseConnection") DatabaseConnection databaseConnection, @WebParam (name = "username") String username)
    {
        try {
            return UserAccessDAO.getUserByUsername(databaseConnection, username).getEmailAddress();
        } catch (Exception ex) {
            Logger.getLogger(EmailAddressService.class.getName()).log(Level.SEVERE, null, ex);
            return "ERROR";
        }
    }
}
