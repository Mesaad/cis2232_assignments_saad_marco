package info.hccis.admin.web;

import info.hccis.admin.model.CalculatorData;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.Deliverable;
import info.hccis.admin.service.DeliverableService;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebGradeCalculatorController {

    private final DeliverableService deliverableService;

    @Autowired
    public WebGradeCalculatorController(DeliverableService deliverableService) {
        this.deliverableService = deliverableService;
    }

    @RequestMapping("/WebGradeCalculator/databaseSetup")
    public String databaseDisplay(Model model, HttpSession session, DatabaseConnection dbConnectionIn) {
        System.out.println("Database provided=" + dbConnectionIn.getDatabaseName());
        model.addAttribute("db", dbConnectionIn);
        session.setAttribute("db", dbConnectionIn);
        ArrayList<Deliverable> ds = deliverableService.getDeliverables(dbConnectionIn);
        model.addAttribute("deliverables", ds);
        return "/WebGradeCalculator/deliverables";
    }
    
    @RequestMapping("/WebGradeCalculator/addToDatabasePage")
    public String databaseAddDisplay(Model model, HttpSession session) {
        Deliverable d = new Deliverable();
        model.addAttribute("deliverable", d);
        return "/WebGradeCalculator/addDeliverables";
    }
    
    @RequestMapping("/WebGradeCalculator/addToDatabase")
    public String databaseAdd(Model model, HttpSession session, @Valid @ModelAttribute("deliverable") Deliverable d) {
        DatabaseConnection dc = (DatabaseConnection)session.getAttribute("db");
        System.out.println("d.name =" + d.name + "d.weight=" + d.weight);
        deliverableService.addDeliverable(dc, d);
        ArrayList<Deliverable> ds = deliverableService.getDeliverables(dc);
        model.addAttribute("deliverables", ds);
        return "/WebGradeCalculator/deliverables";
    }
    
    @RequestMapping("/WebGradeCalculator/calculatorPage")
    public String calculatorDisplay(Model model, HttpSession session) {
        DatabaseConnection dc = (DatabaseConnection)session.getAttribute("db");
        ArrayList<Deliverable> ds = deliverableService.getDeliverables(dc);
        model.addAttribute("deliverables", ds);
        
        CalculatorData cd = new CalculatorData(new int[ds.size()]);
        int index = 0;
        for (Iterator<Deliverable> it = ds.iterator(); it.hasNext();)
        {
            it.next();
            cd.calculationArray[index] = 0;
            index++;
        }
        model.addAttribute("calculationData", cd);
        
        return "/WebGradeCalculator/calculator";
    }
    
    @RequestMapping("/WebGradeCalculator/calculate")
    public String calculate(Model model, HttpSession session, 
            @Valid @ModelAttribute("calculationData") CalculatorData cd) {
        DatabaseConnection dc = (DatabaseConnection)session.getAttribute("db");
        ArrayList<Deliverable> ds = deliverableService.getDeliverables(dc);
        double result = 0;
        for (int index = 0; index < ds.size();index++)
        {
            result += cd.calculationArray[index] * ds.get(index).weight / 100;
        }
        model.addAttribute("result", result);
        return "/WebGradeCalculator/result";
    }

  /*  
    @RequestMapping("/codes/codeTypes")
    public String showCodes(Model model, HttpSession session) {

        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        ArrayList<CodeType> codes = codeService.getCodeTypes(databaseConnection);
        model.addAttribute("codeTypes", codes);
        return "/codes/codeTypes";
    }

    @RequestMapping("/codes/codeValues")
    public String showCodeValues(Model model, HttpServletRequest request) {
        String id = request.getParameter("id");
        System.out.println("id passed in to controller is:" + id);
        model.addAttribute("codeTypeId", id);
        //Get the code values 
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        ArrayList<CodeValue> theList = CodeValueDAO.getCodeValues(dbConnection, id);
        model.addAttribute("codeValues", theList);

        return "/codes/codeValues";
    }

    @RequestMapping("/codes/codeValueAdd")
    public String codeValueAdd(Model model, HttpServletRequest request) {
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        String id = request.getParameter("id");
        System.out.println("BJM in codeValueAdd, id passed in to controller is:" + id);
        CodeValue cv = new CodeValue();
        cv.setCodeTypeId(Integer.parseInt(id));
        model.addAttribute("codeValue", cv);
        return "/codes/codeValueEdit";
    }

    
    @RequestMapping("/codes/codeValueEdit")
    public String codeValueEdit(Model model, HttpServletRequest request) {
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        String id = request.getParameter("id");
        String sequence = request.getParameter("sequence");
        System.out.println("BJM in codeValueEdit, id passed in to controller is:" + id);
        CodeValue cv = CodeValueDAO.getCodeValue(dbConnection, id, sequence);
        model.addAttribute("codeValue", cv);
        return "/codes/codeValueEdit";
    }

    @RequestMapping("/codes/codeValueEditSubmit")
    public String codeValueEditSubmit(Model model, HttpServletRequest request, CodeValue codeValue) {
        System.out.println("Made it to code value edit submit, description=" + codeValue.getDescription());
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        CodeValueDAO.addUpdate(dbConnection, codeValue);

        //next send them back to the code values for the code type.
        ArrayList<CodeValue> theList = CodeValueDAO.getCodeValues(dbConnection, String.valueOf(codeValue.getCodeTypeId()));
        model.addAttribute("codeValues", theList);
        
        model.addAttribute("codeTypeId", codeValue.getCodeTypeId());
        //send to the codeValues view.
        return "/codes/codeValues";
    }
*/
}
