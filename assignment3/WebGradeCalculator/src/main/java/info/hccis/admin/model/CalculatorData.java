/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model;

import javax.persistence.Transient;








public class CalculatorData {
    
    @Transient
    public int[] calculationArray;

    public int[] getCalculationArray() {
        return calculationArray;
    }

    public void setCalculationArray(int[] calculationArray) {
        this.calculationArray = calculationArray;
    }
    
    public CalculatorData(){
        this.calculationArray = null;
    }
    public CalculatorData(int[] ca){
        this.calculationArray = ca;
    }
}
