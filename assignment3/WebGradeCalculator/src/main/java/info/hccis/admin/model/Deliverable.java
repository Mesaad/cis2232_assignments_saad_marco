package info.hccis.admin.model;

public class Deliverable {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
    public String name;
    public int weight;

    public Deliverable(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }
    public Deliverable(){
        this.name = "";
        this.weight = 0;
    }

    @Override
    public String toString() {
        return "Deliverable{" + "name=" + name + ", weight=" + weight + '}';
    }
    
    
}
