package info.hccis.admin.dao;

import info.hccis.admin.model.CodeType;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Purpose: this class adds the CodeType object to the database
 * 
 * @return
 * @author Jordan Campbell
 * @since 20151002
 */
public class CodeTypeDAO {
    
    
    /**
     * Get all of the code types.
     * 
     * @param codeTypeId
     * @return List of CodeType's
     * @since 20151015
     * @author BJ MacLean
     */
    public static ArrayList<CodeType> getCodeTypes(DatabaseConnection databaseConnection) {
        ArrayList<CodeType> codes = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "SELECT * FROM `code_type` order by code_type" ;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getString(2);
                
                CodeType codeType = new CodeType();
                codeType.setCodeType(rs.getInt("code_type"));
                codeType.setEnglishDescription(rs.getString("english_description"));
                codeType.setFrenchDescription(rs.getString("french_description"));
                System.out.println("Found code type="+codeType);
                codes.add(codeType);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }

    
    
    public static void add(DatabaseConnection databaseConnection, CodeType codeTypeToAdd, String updateID) {
        try {
            PreparedStatement ps = null;
            String sql = null;
            Connection conn = null;
            
            sql = "INSERT INTO `code_type`(`code_type`, "
                    + "`english_description`, `french_description`,"
                    + " `created_date_time`, `created_user_id`, "
                    + "`updated_date_time`, `updated_user_id`) "
                    + "VALUES (?,?,?,sysdate(),?,sysdate(),?)";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, codeTypeToAdd.getCodeType());
            ps.setString(2, codeTypeToAdd.getEnglishDescription());
            ps.setString(3, codeTypeToAdd.getFrenchDescription());
            //ps.setString(4, codeTypeToAdd.getCreatedTime());
            ps.setString(5, codeTypeToAdd.getCreatedID());
            //ps.setString(6, date.toString());
            ps.setString(7, updateID);
            ps.executeUpdate();

            DbUtils.close(ps, conn);
        } catch (SQLException ex) {
            Logger.getLogger(CodeValueDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}