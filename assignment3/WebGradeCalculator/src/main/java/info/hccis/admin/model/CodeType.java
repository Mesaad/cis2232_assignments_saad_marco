package info.hccis.admin.model;

/**
 * Purpose: Allow functionality to add a codeType to the database
 * 
 * @author Jordan Campbell
 * @since 20151002
 */
public class CodeType {
    
    private int codeType;
    private String englishDescription, frenchDescription, createdID;
    
    /**
     * 
     * @param cdt
     * @param ed
     * @param fd
     * @param ct
     * @param ci 
     */
    public CodeType(int cdt, String ed, String fd, String ct, String ci){
        
        codeType = cdt;
        englishDescription = ed;
        frenchDescription = fd;
        createdID = ci;
    }

    public CodeType() {
    
    }

    //accessor methods
    public int getCodeType() {
        return codeType;
    }

    public void setCodeType(int cdType) {
        this.codeType = cdType;
    }

    public String getEnglishDescription() {
        return englishDescription;
    }

    public void setEnglishDescription(String engDesc) {
        this.englishDescription = engDesc;
    }

    public String getFrenchDescription() {
        return frenchDescription;
    }

    public void setFrenchDescription(String freDesc) {
        this.frenchDescription = freDesc;
    }

    public String getCreatedID() {
        return createdID;
    }

    public void setCreatedID(String createdID) {
        this.createdID = createdID;
    }

    @Override
    public String toString() {
        return "CodeType{" + "codeType=" + codeType + ", englishDescription=" + englishDescription + ", frenchDescription=" + frenchDescription + ", createdID=" + createdID + '}';
    }
    
    
}