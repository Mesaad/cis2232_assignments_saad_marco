/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service;

import info.hccis.admin.dao.DeliverableDAO;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.Deliverable;
import java.util.ArrayList;
import org.springframework.stereotype.Service;

@Service
public class DeliverableServiceImpl implements DeliverableService{

    @Override
    public void addDeliverable(DatabaseConnection databaseConnection, Deliverable d) {
        DeliverableDAO.addDeliverable(databaseConnection, d);
    }

    /*
    @Override
    public ArrayList<Deliverable> getDeliverables() {
       return DeliverableDAO.getDeliverables(null);
    }
    */

    @Override
    public ArrayList<Deliverable> getDeliverables(DatabaseConnection databaseConnection) {
        
        return DeliverableDAO.getDeliverables(databaseConnection);
    }
    
}
