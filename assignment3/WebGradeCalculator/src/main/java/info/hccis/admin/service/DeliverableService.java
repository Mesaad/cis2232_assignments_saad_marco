/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service;

import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.Deliverable;
import java.util.ArrayList;

public interface DeliverableService {
    //public abstract ArrayList<Deliverable> getDeliverables();
    public abstract ArrayList<Deliverable> getDeliverables(DatabaseConnection databaseConnection);
    public abstract void addDeliverable(DatabaseConnection databaseConnection, Deliverable d);
}
