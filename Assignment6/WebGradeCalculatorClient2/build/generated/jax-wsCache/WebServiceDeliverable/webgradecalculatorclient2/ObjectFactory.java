
package webgradecalculatorclient2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webgradecalculatorclient2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListResponse_QNAME = new QName("http://services.web.admin.hccis.info/", "listResponse");
    private final static QName _List_QNAME = new QName("http://services.web.admin.hccis.info/", "list");
    private final static QName _List1Response_QNAME = new QName("http://services.web.admin.hccis.info/", "list1Response");
    private final static QName _List1_QNAME = new QName("http://services.web.admin.hccis.info/", "list1");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webgradecalculatorclient2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link List }
     * 
     */
    public List createList() {
        return new List();
    }

    /**
     * Create an instance of {@link ListResponse }
     * 
     */
    public ListResponse createListResponse() {
        return new ListResponse();
    }

    /**
     * Create an instance of {@link CodeValue }
     * 
     */
    public CodeValue createCodeValue() {
        return new CodeValue();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.web.admin.hccis.info/", name = "listResponse")
    public JAXBElement<ListResponse> createListResponse(ListResponse value) {
        return new JAXBElement<ListResponse>(_ListResponse_QNAME, ListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.web.admin.hccis.info/", name = "list")
    public JAXBElement<List> createList(List value) {
        return new JAXBElement<List>(_List_QNAME, List.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.web.admin.hccis.info/", name = "list1Response")
    public JAXBElement<ListResponse> createList1Response(ListResponse value) {
        return new JAXBElement<ListResponse>(_List1Response_QNAME, ListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.web.admin.hccis.info/", name = "list1")
    public JAXBElement<List> createList1(List value) {
        return new JAXBElement<List>(_List1_QNAME, List.class, null, value);
    }

}
