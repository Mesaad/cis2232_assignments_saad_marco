package info.hccis.admin.dao;


import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.jpa.Deliverable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;


/**
 *
 * @author BJ
 */
public class DeliverableDAO {
//public static void loadCodes(HttpServletRequest request){
//    
//    request.getSession().setAttribute("organizations",getCodeValues("1"));
//    request.getSession().setAttribute("statuses",getCodeValues("2"));
//    request.getSession().setAttribute("salutations",getCodeValues("3"));
//    request.getSession().setAttribute("circulation_groups",getCodeValues("4"));
//    request.getSession().setAttribute("provinces",getCodeValues("5"));
//    request.getSession().setAttribute("genders",getCodeValues("6"));
//    request.getSession().setAttribute("countries",getCodeValues("7"));
//    request.getSession().setAttribute("programs",getCodeValues("8"));
//    request.getSession().setAttribute("employment_statuses",getCodeValues("9"));
//    request.getSession().setAttribute("currencies",getCodeValues("10"));
//    request.getSession().setAttribute("practice_areas",getCodeValues("11"));
//    request.getSession().setAttribute("employment_statuses",getCodeValues("12"));
//    request.getSession().setAttribute("employment_categories",getCodeValues("13"));
//    request.getSession().setAttribute("funding_sources",getCodeValues("14"));
//    request.getSession().setAttribute("positions",getCodeValues("15"));
//    request.getSession().setAttribute("notification_types",getCodeValues("16"));
//    request.getSession().setAttribute("user_types",getCodeValues("17"));
//    
//    return;
//}    
//
//public static CodeValue getCodeValueFromSession(HttpServletRequest request, String collectionName, int codeSequenceValue){
//    
//    ArrayList<CodeValue> theList = (ArrayList<CodeValue>) request.getSession().getAttribute(collectionName);
//    boolean found = false;
//    int location = 0;
//    CodeValue theCodeValue = null;
//    while (!found && location < theList.size()){
//        if (theList.get(location).getCodeValueSequence() == codeSequenceValue){
//            found = true;
//            theCodeValue = theList.get(location);
//        }
//        location++;
//    }
//    
//    return theCodeValue;
//} 

    public static ArrayList<Deliverable> getDeliverables(DatabaseConnection databaseConnection) {
        ArrayList<Deliverable> deliverableList = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "SELECT * FROM `Deliverable`";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getSTring(2);
                Deliverable d = new Deliverable(rs.getInt("id"),rs.getString("name"),rs.getInt("weight"));
               
                deliverableList.add(d);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return deliverableList;

    }
    
     public static void addDeliverable(DatabaseConnection databaseConnection, Deliverable d) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
          
            sql = "INSERT INTO Deliverable (Name, Weight) VALUES (?, ?);";
            System.out.println("d.name =" + d.name + "d.weight=" + d.weight);
            
            ps = conn.prepareStatement(sql);
            ps.setString(1, d.name);
            ps.setInt(2, d.weight);
          
            ps.executeUpdate();
            System.out.println("try finished");
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }


    }
/*
    public static CodeValue getCodeValue(DatabaseConnection databaseConnection, String codeTypeId, String codeValueSequence) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        CodeValue codeValue = new CodeValue();

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "SELECT * FROM `code_value` WHERE code_type_id = " + codeTypeId
                    + " and code_value_sequence=?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(codeValueSequence));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getSTring(2);
                codeValue.setCodeTypeId(Integer.parseInt(codeTypeId));
                codeValue.setCodeValueSequence(rs.getInt("code_value_sequence"));
                codeValue.setDescription(rs.getString("english_description"));
                codeValue.setDescriptionShort(rs.getString("english_description_short"));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codeValue;

    }

    public static String getCodeValueDescription(DatabaseConnection databaseConnection, int codeTypeId, int sequence) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        String description = "";
        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "SELECT * FROM `code_value` WHERE code_type_id = " + codeTypeId + " and code_value_sequence = " + sequence;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                description = rs.getString("english_description");
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return description;

    }

    public static void addUpdate(DatabaseConnection databaseConnection, CodeValue codeValueToAdd) {
        try {
            PreparedStatement ps = null;
            String sql = null;
            Connection conn = null;
            int maxID = 0;
            boolean adding = false;
            try {
                conn = ConnectionUtils.getConnection(databaseConnection);

                if (codeValueToAdd.getCodeValueSequence() == 0) {
                    adding = true;
                    sql = "SELECT max(code_value_sequence) FROM `code_value` WHERE code_type_id = " + codeValueToAdd.getCodeTypeId();

                    ps = conn.prepareStatement(sql);
                    ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        codeValueToAdd.setCodeValueSequence((rs.getInt(1)+1));
                    }
                }
            } catch (Exception e) {
                String errorMessage = e.getMessage();
                e.printStackTrace();
            }

            if (adding) {

                sql = "INSERT INTO `code_value`(`code_type_id`, `code_value_sequence`, "
                        + "`english_description`, `english_description_short`, `french_description`,"
                        + " `french_description_short`, `created_date_time`, `created_user_id`, "
                        + "`updated_date_time`, `updated_user_id`) "
                        + "VALUES (?,?,?,?,?,?,sysdate(),'admin',sysdate(),'admin')";
                
                ps = conn.prepareStatement(sql);
                ps.setInt(1, codeValueToAdd.getCodeTypeId());
                ps.setInt(2, codeValueToAdd.getCodeValueSequence());
                ps.setString(3, codeValueToAdd.getDescription());
                ps.setString(4, codeValueToAdd.getDescriptionShort());
                ps.setString(5, codeValueToAdd.getDescription() + "FR");
                ps.setString(6, codeValueToAdd.getDescriptionShort() + "FR");
            } else {
                sql = "UPDATE code_value SET english_description=?,english_description_short=?,french_description=?,french_description_short=? WHERE code_type_id = ? and code_value_sequence=?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, codeValueToAdd.getDescription());
                ps.setString(2, codeValueToAdd.getDescriptionShort());
                ps.setString(3, codeValueToAdd.getDescription() + "FR");
                ps.setString(4, codeValueToAdd.getDescriptionShort() + "FR");
                ps.setInt(5, codeValueToAdd.getCodeTypeId());
                ps.setInt(6, codeValueToAdd.getCodeValueSequence());

            }

            ps.executeUpdate();

            DbUtils.close(ps, conn);
        } catch (SQLException ex) {
            Logger.getLogger(DeliverableDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
*/
    /**
     * This method will delete the row that the user wants to delete.
     *
     * @author Zyler Fenner
     * @since 20151002
     *
     * @param codeType
     * @param codeValue
     */
    
    /*
    public static void delete(DatabaseConnection databaseConnection, int codeTypeId, int codeValueSequence) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "DELETE FROM `code_value` "
                    + "WHERE `code_type_id` = " + codeTypeId
                    + "AND 'code_value_sequence' = " + codeValueSequence;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
    }
*/
    /**
     * @author Connor Oliver
     * @param searchTerm
     * @return
     */
    /*
    public static ArrayList<CodeValue> find(DatabaseConnection databaseConnection, String searchTerm) {
        //Connor Oliver
        ArrayList<CodeValue> codes = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM code_value WHERE english_description like '" + searchTerm + "'";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codes.add(new CodeValue(rs.getString("english_description"),
                        rs.getString("english_description_short"),
                        rs.getInt("code_type_id"),
                        rs.getInt("code_value_sequence")));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;
 
    }
   */
}
