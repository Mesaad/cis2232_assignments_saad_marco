/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.web.services;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author bjmaclean
 */
@WebService(serviceName = "UserAccess")
public class UserAccess {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "validate")
    public int validate(@WebParam(name = "database") String database, @WebParam(name = "username") String username, @WebParam(name = "password") String password) {
        return 1;
    }

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "getHashPassword")
    public String getHashPassword(@WebParam(name = "passwordIn") String passwordIn) {
        return passwordIn+"HASHED";
    }



}
