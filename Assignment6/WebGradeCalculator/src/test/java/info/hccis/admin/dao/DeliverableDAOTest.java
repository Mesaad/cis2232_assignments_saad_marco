/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.dao;

import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.Deliverable;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Interweb
 */
public class DeliverableDAOTest {
    
    public DeliverableDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDeliverables method, of class DeliverableDAO.
     */
    @Test
    public void testGetDeliverables() {
        System.out.println("getDeliverables");
        DatabaseConnection databaseConnection = new DatabaseConnection();
        ArrayList<Deliverable> expResult = new ArrayList<Deliverable>();
        expResult.add(new Deliverable(1,"Assignment 1",20));
        expResult.add(new Deliverable(2,"Assignment 2",20));
        expResult.add(new Deliverable(3,"Final Exam",60));
        ArrayList<Deliverable> result = DeliverableDAO.getDeliverables(databaseConnection);
        System.out.println(expResult.toString());
        System.out.println(result.toString());
        assertEquals(expResult.toString(), result.toString());
        
    }


}
