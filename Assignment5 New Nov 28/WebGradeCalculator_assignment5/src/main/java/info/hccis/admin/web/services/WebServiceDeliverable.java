/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.web.services;

import info.hccis.admin.data.springdatajpa.DeliverableRepository;
import info.hccis.admin.model.jpa.Deliverable;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Sony
 */
@WebService(serviceName = "WebServiceDeliverable")
public class WebServiceDeliverable {

    /**
     * This is a sample web service operation
     */
    private final DeliverableRepository dr;
    
    @Autowired
    public WebServiceDeliverable(DeliverableRepository dr)
    {
        this.dr = dr;
    }
    
    @WebMethod(operationName = "getDeliverables")
    public ArrayList<Deliverable> getDeliverables() {
        return (ArrayList<Deliverable>)dr.findAll();
    }
}
